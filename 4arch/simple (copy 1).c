#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>



static __inline__ unsigned long long getticks(void)
{
     unsigned a, d;
     asm volatile("rdtsc" : "=a" (a), "=d" (d));
     return (((unsigned long long)a) | (((unsigned long long)d) << 32));
}

#define BILLION 1E9
 
int main()
{
  int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
   26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
   51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
   76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99};
        unsigned long long tick,tick1;

        for(int q=0; q<20; q++){

struct timespec requestStart, requestEnd;
clock_gettime(CLOCK_REALTIME, &requestStart);

    for(int i=0; i<99; i++)
      for(int j=i+1; j<100; j++)
        if(arr[i] > arr[j]){
          int temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }

clock_gettime(CLOCK_REALTIME, &requestEnd);
long long accum = ( requestEnd.tv_sec - requestStart.tv_sec )*BILLION
  + ( requestEnd.tv_nsec - requestStart.tv_nsec );
      printf("Result %2d: %llu\n", q+1, accum);

                  }


}