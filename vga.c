#include "vga.h"
 
  
const size_t VGA_WIDTH = 80;
const size_t VGA_HEIGHT = 50;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

uint8_t make_color(enum vga_color fg, enum vga_color bg) 
{
	return fg | bg << 4;
}
 
uint16_t make_vgaentry(char c, uint8_t color) 
{
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}
 
 void terminal_initialize() {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_WHITE, COLOR_BLACK);
	terminal_buffer = (uint16_t*) 0xB8000;
	
	for (size_t x = 0; x < VGA_WIDTH; x++) {
		for (size_t y = 0; y < VGA_HEIGHT; y++) {
			const size_t index = x * VGA_WIDTH + y;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
}

size_t strlen(const char* str) 
{
	size_t len = 0;
	while ( str[len] != 0 )
		len++;
	return len;
}

void terminal_putentryat(const char c, uint8_t color, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

void terminal_move_up()
{
	int from = VGA_WIDTH, to = 0;
	int end = VGA_WIDTH*VGA_HEIGHT;

	while(from < end)
		terminal_buffer[to++] = terminal_buffer[from++];

	while(to++ < end)
		make_vgaentry(' ', terminal_color);
}

void terminal_putchar(char c)
{
	if(c == '\n')
	{
		terminal_row++;
		terminal_column = 0;
		if(terminal_row == VGA_HEIGHT)
		{
			terminal_move_up();
			terminal_row--;
		}
		return;
	}

	terminal_putentryat(c, terminal_color, terminal_column, terminal_row);

	if ( ++terminal_column == VGA_WIDTH )
	{
		terminal_column = 0;
		if ( ++terminal_row == VGA_HEIGHT )
		{
			terminal_row = 0;
		}
	}
}

int terminal_get_pos(){
    return terminal_row*VGA_WIDTH + terminal_column;
}

void terminal_writestring(const char* ptr, int len){
  for (int i = 0; i < len; i++) {
    terminal_putchar (*ptr++);
  }
}