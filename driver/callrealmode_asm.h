﻿#pragma once

#define CALLREALMODE_OFFSET 0x5000 // адрес, по которому копируется 16битный код

// типы диапазонов физической памяти
// диапазоны с типом 0x1 доступны для использования
#define SYSMEMMAP_TYPE_AVAILABLE    0x1
#define SYSMEMMAP_TYPE_RESERVED     0x2
#define SYSMEMMAP_TYPE_ACPI_RECLAIM 0x3
#define SYSMEMMAP_TYPE_ACPI_NVS     0x4

enum callrealmode_Func 
{
    CALLREALMODE_FUNC_GETSYSMEMMAP = 0,
    CALLREALMODE_FUNC_READ_DISK    = 1,
    CALLREALMODE_FUNC_WRITE_DISK   = 2
};

struct callrealmode_GetSysMemMap
{
    unsigned int num;
    unsigned int next_num;
    unsigned long long base;
    unsigned long long len;
    unsigned int type;
}  __attribute__ ((packed)); // структура выравнена по 1 байту

struct callrealmode_read_disk
{
    unsigned long long start_sector_lba;
    unsigned int buff_addr;
    unsigned int sectors_count;
    unsigned short disk_number;
    unsigned char ret_code;
} __attribute__ ((packed));

// эта структура передается функции 
// callrealmode_Call и возвращается ей.
struct callrealmode_Data
{
    enum callrealmode_Func func : 16;
    union 
    {
        // здесь удобно добавлять новые типы данных 
        // для новых типов вызовов.
        struct callrealmode_GetSysMemMap getsysmemmap;
        struct callrealmode_read_disk readdisk;
    };
} __attribute__ ((packed)); // структура выравнена по 1 байту

// метки из файла callrealmode_asm.s, обрамляющие 
// код на asm. Используются для копирования.
extern char callrealmode_start;
extern char callrealmode_end;