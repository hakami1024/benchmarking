#include "desc.h"
#include "string.h"
#include "segment.h"
#include <stdint.h>

static void SetSegDesc(struct segdesc *d, unsigned int limit, unsigned int base, enum segdesc_type type,
                       enum segdesc_s s, unsigned int dpl, unsigned int p,
                       unsigned int avl, enum segdesc_l l, enum segdesc_d_b d_b)
{
    d->base_15_0 = base;
    d->base_23_16 = base >> 16;
    d->type = type;
    d->s = s;
    d->dpl = dpl;
    d->p = p;
    d->avl = avl;
    d->l = l;
    d->d_b = d_b;
    d->base_31_24 = base >> 24;
    
    if (limit <= 0xFFFFF) 
    {
        d->g = 0;
        d->limit_15_0 = limit >> 0;
        d->limit_19_16 = limit >> 16;
    } 
    else 
    {
        d->g = 1;
        d->limit_15_0 = limit >> 12;
        d->limit_19_16 = limit >> 28;
    }
} 
#define set_cs( cs ) __asm__ volatile ( "lcall %0, $fake_label \n\t"\
  "jmp after_label \n\t"\
  "fake_label: \n\t"\
  " mov $0x00000005, %%eax \n\t"\
  " pop %%eax \n\t"\
  " pop %%ebx \n\t"\
  " push %0 \n\t"\
  " push %%eax \n\t"\
  " lret \n\t"\
  " after_label:\n\t"\
 :: "i" (cs) );

void SetupDescTables(struct segdesc *GDT_base)
{
    // SEG_SEL_NULL
    memset(&GDT_base[0], 0, sizeof(GDT_base[0]));       // нулевой сегмент. всегда 0
    
    // SEG_SEL_CODE32 
    SetSegDesc(&GDT_base[1], 0xFFFFFFFF, 0x00000000,    // 32х битный сегмент кода
             SEGDESC_TYPE_EXECREAD_CODE,                // уровень привилегий 0
             SEGDESC_S_CODE_OR_DATA_SEGMENT, 0, 1,      // база 0 лимит 4G
             0, SEGDESC_L_16_OR_32, SEGDESC_D_B_32);
    
    // SEG_SEL_DATA32
    SetSegDesc(&GDT_base[2], 0xFFFFFFFF, 0x00000000,    // 32х битный сегмент данных
             SEGDESC_TYPE_RDWR_DATA,                    // уровень привилегий 0
             SEGDESC_S_CODE_OR_DATA_SEGMENT, 0, 1,      // база 0 лимит 4G
             0, SEGDESC_L_16_OR_32, SEGDESC_D_B_32);

    // SEG_SEL_CODE16
    SetSegDesc(&GDT_base[3], 0x0000FFFF, 0x00000000,    // 16ти битный сегмент кода
             SEGDESC_TYPE_EXECREAD_CODE,                // уровень привилегий 0
             SEGDESC_S_CODE_OR_DATA_SEGMENT, 0, 1,      // база 0 лимит 4G
             0, SEGDESC_L_16_OR_32, SEGDESC_D_B_16);

    // SEG_SEL_DATA16
    SetSegDesc(&GDT_base[4], 0x0000FFFF, 0x00000000,    // 16ти битный сегмент данных
             SEGDESC_TYPE_RDWR_DATA,                    // уровень привилегий 0
             SEGDESC_S_CODE_OR_DATA_SEGMENT, 0, 1,      // база 0 лимит 4G
             0, SEGDESC_L_16_OR_32, SEGDESC_D_B_16);

    struct descreg gdtr;
    
    /*
    struct descreg *gdtr_first;
    
    __asm__ __volatile__("sgdt %0":"=m"(*gdtr_first)::"memory");
    
    printf("addr = %x, limit = %x\n", gdtr_first, gdtr_first->limit);
     
    for(int i=0; i<(gdtr_first->limit+1)/sizeof(struct segdesc); i++)
    {
      struct segdesc d = ((struct segdesc *)gdtr_first->base)[i];
      printf("%d) base = %d %d %d, limit = %d %d, type = %d \n", i, d.base_15_0, d.base_23_16, d.base_31_24, 
        d.limit_15_0, d.limit_19_16, d.type);
    }
    */

    gdtr.base = (unsigned long)GDT_base;            // указатель на сформированную таблицу
    //printf("gdt entry size: %d\n", sizeof(*GDT_base));
    gdtr.limit = 5 * sizeof(*GDT_base) - 1; // размер таблицы в байтах - 1
    
    //printf("\nlimit = %d, %x\n", gdtr.limit, gdtr.limit);
    // printf("num = %d\n", 5 * sizeof(*GDT_base) - 1);
    // printf("sizeof = %d\n", sizeof(*GDT_base));
   
   /*
    __asm__ volatile (
        "mov %0,%%ds \n"    // Загружаем 32-битный селектор данных в 
        "mov %0,%%es \n"    // регистры DS, ES, FS, GS, SS
        "mov %0,%%fs \n"    //
        "mov %0,%%gs \n"    //
        "mov %0,%%ss \n"    //
        :: "b" ((uint32_t)0x18) 
        );
        
    set_cs(0x10); 
    */

    __asm__ volatile ("lgdt %0"             // GCC-Inline-Assembly
                      :
                      : "m" (gdtr));
                      
    // __asm__ __volatile__("sgdt %0":"=m"(*gdtr_first)::"memory");
    
    /*
    printf("addr = %x, limit = %x\n", gdtr_first, gdtr_first->limit);
     
    for(int i=0; i<(gdtr_first->limit+1)/sizeof(struct segdesc); i++)
    {
      struct segdesc d = ((struct segdesc *)gdtr_first->base)[i];
      printf("%d) base = %d %d %d, limit = %d %d \n", i, d.base_15_0, d.base_23_16, d.base_31_24, d.limit_15_0, d.limit_19_16);
    }
    */
         
    __asm__ volatile (
        "mov %0,%%ds \n"    // Загружаем 32-битный селектор данных в 
        "mov %0,%%es \n"    // регистры DS, ES, FS, GS, SS
        "mov %0,%%fs \n"    //
        "mov %0,%%gs \n"    //
        "mov %0,%%ss \n"    //
        :: "b" ((uint32_t)SEG_SEL_DATA32) 
        );
        
    set_cs((uint32_t)SEG_SEL_CODE32);
}
