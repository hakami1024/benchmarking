
#include <stdint.h>
#include "string.h"
#include "segment.h"
#include "callrealmode_asm.h"

#include "../fat_io_lib/fat_opts.h"
#include "mbr.h"

uint64_t g_BootPartitionStart = 0;   // номер первого сектора раздела с файловой системой
uint32_t g_BootDeviceInt13Num = 0;   // номер загрузочного диска

// interrupts and paging must be disabled 
static void callrealmode_Call(struct callrealmode_Data *p_param)
{
    uint16_t sp16;
    uint32_t sp32;
    
    // copy 16 bit code and stack
    // первый вызов memcpy копирует 16ти битный код между 
    // метками callrealmode_start и callrealmode_end
    // по адресу CALLREALMODE_OFFSET < 1Mb. Это делается 
    // из за того что, код в RM не может обращаться к 
    // памяти выше 1Mb.
    //printf("In callrealmode\n");
    //printf("Going to copy memory: start=%x, len=%x\n", &callrealmode_start, &callrealmode_end - &callrealmode_start);
    //wait();
    memcpy ((uint8_t*)CALLREALMODE_OFFSET, &callrealmode_start, 
            &callrealmode_end - &callrealmode_start);
    // перед кодом располагаем стек, который не будет затирать код, так как растет вниз.
    // параметры для вызова в RM располагаются на стеке, и новое значение регистра SP 
    // вычисляется с учетом этого
    //printf("Moving params\n");
    //wait();
    
    sp16 = CALLREALMODE_OFFSET - sizeof(*p_param);
    // второй вызов memcpy копирует параметры на стек
    memcpy ((void*)(uint32_t)sp16, p_param, sizeof(*p_param));
    
    //printf("Running asm, offset = %x\n", CALLREALMODE_OFFSET);
    //wait();
    
    __asm__ volatile (
        "mov %%esp,%0\n"    // Сохраняем ресистр ESP в переменную sp32
        "mov %1,%%ds \n"    // Загружаем 16ти битный селектор данных в 
        "mov %1,%%es \n"    // регистры DS, ES, FS, GS, SS
        "mov %1,%%fs \n"    //
        "mov %1,%%gs \n"    //
        "mov %1,%%ss \n"    //
        "mov %2,%%esp\n"    // Переходим на 16ти битный стек по адресу sp16
        "pusha       \n"    // Сохраняем регистры общего назначения
        "lcall %3, %4 \n"    // Загружаем 16ти битный селектор кода в CS 
                            // и переходим на адрес CALLREALMODE_OFFSET.
                            // На стеке сохраняется текущий CS и EIP,
                            // которые будут использованны инструкцией 
                            // lretl для возврата в 32х битный код
        "popa        \n"    // востанавливаем сохраненные регистры общего назначения
        "mov %5,%%ds \n"    // Загружаем 32х битный селектор данных в 
        "mov %5,%%es \n"    // регистры DS, ES, FS, GS, SS
        "mov %5,%%fs \n"    //
        "mov %5,%%gs \n"    //
        "mov %5,%%ss \n"    //
        "mov %0,%%esp\n"    // Переходим на 32х битный стек, адрес
                            // которого сохранен в переменной sp32
        : "=&a" (sp32)              // %0 – Input
        : "b" ((uint32_t)SEG_SEL_DATA16) // %1 - Output
        , "c" ((uint32_t)sp16)           // %2 - Output
        , "i" ((uint16_t)SEG_SEL_CODE16) // %3 - Output
        , "i" (CALLREALMODE_OFFSET) // %4 - Output
        , "d" ((uint32_t)SEG_SEL_DATA32) // %5 - Output
        );
        
    //printf("AFTER ASM\n");
    //wait();

    // копируем с результаты работы с 16ти битного стека в p_param
    memcpy (p_param, (void*)(uint32_t)sp16, sizeof(*p_param));
}

uint64_t GetAvalibleRAMSize()
{
	//wait();
    struct callrealmode_Data param; // структура, в которой передаются параметры 
                                    // для кода в RM, и возвращается результат
    uint64_t avalible_ram_sz = 0;

    param.func = CALLREALMODE_FUNC_GETSYSMEMMAP;
    param.getsysmemmap.next_num = 0;
    //printf("in GetAvailableRAMSize\n\n");

    do
    {
    	//wait();
        param.getsysmemmap.num = param.getsysmemmap.next_num;
        callrealmode_Call(&param);  // int 0x15, где EBX = param.getsysmemmap.num
                                    //   EAX = 0xE820, EDX = 0x534d4150, ECX = 20
                                    //   ES:DI = param.getsysmemmap.base
                                    // после вызова EBX сохраняется
                                    //   param.getsysmemmap.next_num = EBX
        //printf("Survived callrealmode\n");
        //wait();

        // нас интересуют только доступные диапазоны с типом SYSMEMMAP_TYPE_AVAILABLE
        if (SYSMEMMAP_TYPE_AVAILABLE == param.getsysmemmap.type)
        {
            avalible_ram_sz += param.getsysmemmap.len;
        }
        
        
        printf("n 0x%08X nn 0x%08X b 0x%08llX l 0x%08llX(%lldMb) t 0x%08X\n",
            param.getsysmemmap.num, 
            param.getsysmemmap.next_num,
            param.getsysmemmap.base, 
            param.getsysmemmap.len, 
            param.getsysmemmap.len / 0x100000, 
            param.getsysmemmap.type);
        
    }
    while (param.getsysmemmap.next_num);

    return avalible_ram_sz;
}

// Чтение сектора с диска
int ReadBootMedia(unsigned long sector, unsigned char *buffer, unsigned long sectorCount)
{
    struct callrealmode_Data param; 

    param.func = CALLREALMODE_FUNC_READ_DISK;

    void *low_mem_buff = CALLREALMODE_OFFSET + (&callrealmode_end - &callrealmode_start);
    
    for (int i = 0; i < sectorCount; i++)
    {
        param.readdisk.start_sector_lba = sector + g_BootPartitionStart + i;
        param.readdisk.buff_addr        = (uint32_t)low_mem_buff;
        param.readdisk.disk_number      = g_BootDeviceInt13Num;
        param.readdisk.sectors_count    = 1;

        callrealmode_Call(&param);  // int 0x13 с параметрами из "param"
            
        if (param.readdisk.ret_code)
        {
            printf("error reading disk\n");
            return 0;   // error
        }

        memcpy(buffer + i * FAT_SECTOR_SIZE, low_mem_buff, FAT_SECTOR_SIZE);
    }

    return 1;   // success
}   

// Запись сектора на диск. Заглушка
int WriteBootMedia(unsigned long sector, unsigned char *buffer, unsigned long sectorCount)
{
    struct callrealmode_Data param;

    param.func = CALLREALMODE_FUNC_WRITE_DISK;

    void *low_mem_buff = CALLREALMODE_OFFSET + (&callrealmode_end - &callrealmode_start);
    
    for (int i = 0; i < sectorCount; i++)
    {
        memcpy(low_mem_buff, buffer + i * FAT_SECTOR_SIZE, FAT_SECTOR_SIZE);

        param.readdisk.start_sector_lba = sector + g_BootPartitionStart + i;
        param.readdisk.buff_addr        = (uint32_t)low_mem_buff;
        param.readdisk.disk_number      = g_BootDeviceInt13Num;
        param.readdisk.sectors_count    = 1;

        callrealmode_Call(&param);  // int 0x13 с параметрами из "param"
            
        if (param.readdisk.ret_code)
        {
            printf("error writing to disk\n");
            return 0;   // error
        }
    }
    
    return 1;   // success
}   

// Функция инициализации
uint32_t InitBootMedia(uint8_t bootDevice)
{
    g_BootDeviceInt13Num = bootDevice;

    // Читаем первый сектор диска
    MBRSector_t mbr;
    if (ReadBootMedia(0, (uint8_t*)&mbr, 1) == 0)
    {
        printf("Can't read disk's first sector\n");
        return 0;
    }

    // проверяем сигнатуру
    if (mbr.mbr_sign[0] != 0x55 || 
        mbr.mbr_sign[1] != 0xaa)
    {
        printf("Invalid signature, 0x%x, 0x%x\n", mbr.mbr_sign[0], mbr.mbr_sign[1]);
        return 0;
    }
    
    // ищем загрузочный раздел
    int i;
    for (i = 0; i < 4; i++)
    {
        if (mbr.part[i].boot_indicator == 0x80)
        break;
    }
    if (i == 4)
    {
        printf("Boot sector not found.\n");
        return 0;
    }

    // сохраняем номер первого сектора загрузочного раздела
    g_BootPartitionStart = mbr.part[i].start_lva;

    // printf("start sector = %lld boot dev int13 num = 0x%x\n", 
    // g_BootPartitionStart, g_BootDeviceInt13Num);
    return 1;
}