#pragma once

unsigned long long GetAvalibleRAMSize();
unsigned int InitBootMedia(unsigned short bootDevice);
int ReadBootMedia(unsigned long sector, unsigned char *buffer, unsigned long sectorCount);
int WriteBootMedia(unsigned long sector, unsigned char *buffer, unsigned long sectorCount);