#pragma once

struct MBRPartitionEntry
{
	unsigned char  boot_indicator;
	unsigned char  start_head;
	unsigned short start_sector   : 6;
	unsigned short start_cylinder : 10;
	unsigned char  sys_id;
	unsigned char  end_head;
 	unsigned short end_sector   : 6;
  	unsigned short end_cylinder : 10;
	unsigned int   start_lva;
 	unsigned int   size_in_sectors;
} __attribute__ ((packed));

typedef struct MBRPartitionEntry MBRPartitionEntry_t;
    
struct MBRSector
{
  unsigned char code[446];
  MBRPartitionEntry_t part[4];
  unsigned char mbr_sign[2];
} __attribute__ ((packed));

typedef struct MBRSector MBRSector_t;