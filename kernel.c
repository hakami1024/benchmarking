#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
 
/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
 
/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

#include "syscalls.h"
#include "vga.h"
#include "desc.h"
#include "callrealmode.h"
#include "multiboot.h"
#include "fat_io_lib/fat_filelib.h"
#include "kernel.h"


struct segdesc g_GDT[5];

extern int main();

void kernel_main(unsigned int magic, unsigned int mbd) 
{
	terminal_initialize();
	
	if (magic != MULTIBOOT_BOOTLOADER_MAGIC)
	  {
	       printf("Invalid magic number: 0x%x, mbd=0x%x\n", magic, mbd);
	       return;
	   }
	   
	bool have_media = false;

 	SetupDescTables(g_GDT);


	 multiboot_info_t *p_multiboot_info = (multiboot_info_t*)mbd;
	 if (InitBootMedia(p_multiboot_info->boot_device >> 24) == 0)
	 {
	 	printf("Error: InitBootMedia failed.\n");
	 } else {
	 	fl_init();
	 	if (fl_attach_media(ReadBootMedia, WriteBootMedia) != FAT_INIT_OK)
	 	{
	  		printf("Error: Media attach failed.\n");
	  	} 
	  	else {
	  		have_media = true;
	  	}
 	 }                                                            
	 unsigned int cr0;
	 	    // asm volatile ( "mov %%cr0, %0" : "=g"(cr0) );

	 // invalidate the cache
	// write-back and invalidate the cache
	// asm volatile("wbinvd");
 
	// plug cr0 with just PE/CD/NW
	// cache disable(486+), no-writeback(486+), 32bit mode(386+)
	// asm volatile("movl %%eax, %%cr0" : :
		// "g" (cr0 | 0x00000001 | 0x40000000 | 0x20000000) : "eax");

	 // выводим список файлов в папке /boot/grub
	 // if(have_media)
	 // {
	 	// fl_listdirectory("/boot/grub");
	 // }

	 

	// fl_listdirectory("/");

	// file = fl_fopen("/test.txt", "r");
	// if (file == 0)
	// {
		// printf("Error: can not open file for reading.\n");
		// return;
	// }

	// printf("\nContent of the file /test.txt:\n");
	// char str[64];

	// while (fl_fgets(str, sizeof(str), file))
	// {
		// printf("%s", str);
	// }
	// asm volatile ("cli");
 	 unsigned long long delta;

 	 unsigned long long results[20];

 	 for(int i=0; i<20; i++){

	 delta = rdtsc();

	 main();

	 delta = rdtsc() - delta;

	 results[i] = delta;
	}
		// asm volatile ("sti");


		// asm volatile("movl %%eax, %%cr0" : : "g" (cr0) : "eax");

		for(int i=0; i<20; i++){

			
			printf("Result %2d written to file: %llu\n", i+1, results[i]);
			

		}
	

void *file = fl_fopen("/test.txt", "w");

	for(int i=0; i<20; i++){
		const int n = snprintf(NULL, 0, "%llu\n", results[i]);
  	 		 char ans_buf[n+1];

			 snprintf(ans_buf, n+1, "%llu\n", results[i]);
			 fl_fputs(ans_buf, file);

	}
	fl_fclose(file);


	fl_shutdown();
}

