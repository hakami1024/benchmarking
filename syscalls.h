/* note these headers are all provided by newlib - you don't need to provide them */
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/times.h>
#include <sys/errno.h>
#include <sys/time.h>
#include <stdio.h>

#include <errno.h>
#undef errno
extern int errno;

//TODO: provide empty syscalls
 
void _exit();
int close(int file);
char **environ; /* pointer to array of char * strings that define the current environment variables */
int execve(const char *name, const char **argv, const char **env);
int fork();
int fstat(int file, struct stat *st);
int getpid();
int isatty(int file);
int kill(int pid, int sig);
int link( const char *old, const char *new);
int lseek(int file, int ptr,  int mode);
int open( const char *name, int flags, ...);
int read(int file, const char *ptr, int len);
void* sbrk(int incr);
int stat( const char *file, struct stat *st);
clock_t times(struct tms *buf);
int unlink(const char *name);
int wait(int *status);
int write(int file, const char *ptr, int len);
int gettimeofday(struct timeval *p, void *z);