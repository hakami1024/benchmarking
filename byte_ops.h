#pragma once

#include <stdint.h>

static inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

static inline uint16_t inw(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inw %%dx, %%ax" : "=a"(ret) : "d"(port) );
    return ret;
}

static inline void outw(unsigned short port, uint16_t data)
{
   asm volatile("outw %%ax, %%dx" : : "d" (port), "a" (data));
}

static inline unsigned long inl(unsigned short port)
{
   unsigned long result;
   asm volatile("inl %%dx, %%eax" : "=a" (result) : "Nd" (port));
   return result;
}

static inline void outl(unsigned short port, unsigned long data)
{
   asm volatile("outl %%eax, %%dx" : : "d" (port), "a" (data));
}