#pragma once
#define __Benchmarking__
#if !defined(__cplusplus)
#include <stdbool.h> 
#endif
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
 
/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
 
#if !defined(__i386__)
#error "Needs to be compiled with a i686-benchmarking compiler"
#endif

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif

static inline uint64_t rdtsc()
{
    uint64_t ret;
    asm volatile ( 
    "CPUID \n\t"
    "rdtsc" : "=A"(ret) );
    return ret;
}

static inline int empty_printf(const char* restrict format, ...) {}
static inline int empty_puts(const char* str){}

void kernel_main();

