#include "syscalls.h"

void exit(){
// not implemented
}

int close (int   file)
{
  errno = EBADF;
  
  return -1;                    /* Always fails */

} 

char *__env[1] = { 0 };
char **environ = __env;

int execve(const char *name, const char **argv, const char **env) {
  errno = ENOMEM; /* Always fails */
  return -1; 
}

int fork(void) {
  errno = EAGAIN; /* Always fails */
  return -1;
}

#include <sys/stat.h>
int fstat(int file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

int getpid(void) {
  return 1; /* There is a single process */
}

int isatty(int file) {
  return 1; /* Any stream is tty stream */
}

int kill(int pid, int sig) {
  errno = EINVAL; /* Always fails */
  return -1;
}

int link(const char *old, const char *new) {
  errno = EMLINK; /* Always fails */
  return -1;
}

int lseek(int file, int ptr,  int mode) {
  return 0; /* any stream is tty stream and it's always positioned to the beginning */
}

int open(const char *name, int flags, ...) {
  errno = ENOSYS; /* Always fails */
  return -1; 
}

int read(int file, const char *ptr, int len) {
  return 0; /* Never reading anything */
}

uintptr_t kmem_ptr = 0xE0000000;

void* sbrk(int incr)
{
    if(kmem_ptr + incr >= 0xF0000000)
    {
      errno = ENOMEM;
      return 0;
    }

    kmem_ptr = kmem_ptr + incr;
    errno = 0;
    return (void *)(kmem_ptr-incr);
}

int stat(const char *file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

clock_t times(struct tms *buf) {
  errno = EACCES; /* Always fails */
  return -1;
}

int unlink(const char *name) {
  errno = ENOENT; /* Always fails */
  return -1; 
}

int wait (int *status)
{
  errno = ECHILD;
  return -1;                    /* Always fails */

}  

#include "byte_ops.h"
#include "vga.h"


void update_cursor(unsigned short old_pos, unsigned short new_pos)
{
    outb(0x3D4, 0x0F);
    outb(0x3D5, (unsigned char)(old_pos&0xFF));

    outb(0x3D4, 0x0E);
    outb(0x3D5, (unsigned char)(new_pos&0xFF));
}


int write(int file, const char *ptr, int len) {
  int last_pos = terminal_get_pos();
  
  terminal_writestring(ptr, len);
  
  int pos = terminal_get_pos();
  
  //update_cursor(last_pos, pos);
    
  return len;
}

int gettimeofday(struct timeval *p, void *z)
{
   p->tv_sec = 0;
   p->tv_usec = 0;
   return 0;
}
