LOADER_FLAGS=''
KERNEL_FLAGS='-std=gnu99 -ffreestanding -O0'
LD_FLAGS="-T linker.ld -ffreestanding -nostdlib -O1"
STD_LIBS="-lc -lm -lg -lgcc -nostdlib"
 
LOADER_OBJ=loader.o
KERNEL_OBJ=kernel_module.o
ISO=iso/boot/benchmarking.bin
 
echo "compiling $LOADER_OBJ"
i686-benchmarking-as -o loader.o kernel.s 

i686-benchmarking-gcc -Wl,-T -Wl,linker.ld -Wl,-nostdlib \
    -ffreestanding -std=gnu99 -O0 -ggdb3\
    -I driver \
    -I fat_io_lib \
    loader.o \
    program.c \
    kernel.c \
    syscalls.c \
    vga.c \
    driver/callrealmode_asm.s \
    driver/desc.c \
    driver/callrealmode.c \
    fat_io_lib/fat_access.c \
    fat_io_lib/fat_cache.c \
    fat_io_lib/fat_filelib.c \
    fat_io_lib/fat_format.c \
    fat_io_lib/fat_misc.c \
    fat_io_lib/fat_string.c \
    fat_io_lib/fat_table.c \
    fat_io_lib/fat_write.c \
    $STD_LIBS \
    -o iso/boot/benchmarking.bin.dbg

rm *.o 

rm benchmarking.iso


cp iso/boot/benchmarking.bin.dbg iso/boot/benchmarking.bin

strip iso/boot/habraos.bin

dd if=/dev/zero of=benchmarking.iso bs=512 count=131072

(echo n; echo p; echo 1; echo ; echo ; echo a; echo t; echo c; echo w) | fdisk benchmarking.iso

losetup /dev/loop0 benchmarking.iso
losetup /dev/loop1 benchmarking.iso -o 1048576

mkdosfs /dev/loop1

mkdir mount-iso

mount /dev/loop1 mount-iso

cp -r iso/boot mount-iso && sync

grub-install --target=i386-pc --modules="part_msdos biosdisk fat multiboot" --boot-directory=mount-iso/boot -d /usr/lib/grub/i386-pc /dev/loop0

sync

ls mount-iso

ls mount-iso/boot

cp -r iso/boot mount-iso && sync

ls mount-iso

ls mount-iso/boot

umount /dev/loop1

rm -rf mount-iso

losetup -d /dev/loop1
losetup -d /dev/loop0

qemu-system-x86_64 -s -S -hda benchmarking.iso